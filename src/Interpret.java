import java.util.*;
import java.io.*;

public class Interpret {
  public static void main(String[] args)
  {
    if (args.length != 2)
      System.out.println("Usage : java -jar interpret.jar <script_file> <csv_file>");
    else
    {
      ScriptFile script = new ScriptFile(args[0]);
      CSVFile csv = new CSVFile(args[1]);
      List<String> script_file = script.getScriptFile();
      String[][] csv_file = csv.getCSVFile();

      if (csv_file != null)
      {
        if (script_file != null)
        {
          csv.splitAndSave(script_file);
          if (csv.getCSVFile() != null)
            csv.saveFile();
        }
        else
          System.out.println("Bad script file.");
      }
      else
        System.out.println("Bad csv file.");
    }
  }
}
