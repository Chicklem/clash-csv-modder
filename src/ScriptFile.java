import java.util.*;
import java.io.*;

public class ScriptFile {

  private String filename;
  private List<String> script_file;

  ScriptFile(String filename)
  {
    this.filename = filename;
    this.script_file = null;
    readStockScriptFile();
  }

  private void readStockScriptFile()
  {
    File file = new File(this.filename);
    BufferedReader reader = null;
    this.script_file = new ArrayList<String>();

    try
    {
      reader = new BufferedReader(new FileReader(file));
      String line;
      while ((line = reader.readLine()) != null)
        this.script_file.add(line);
      reader.close();
    }
    catch (Exception e)
    {
      System.err.format("Exception occured trying to read '%s'", this.filename);
      e.printStackTrace();
    }
  }

  public List<String> getScriptFile()
  {
    return this.script_file;
  }
}
