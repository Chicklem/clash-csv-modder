import java.util.*;
import java.io.*;

public class CSVFile {

  private String filename;
  private String[][] csv_file;

  CSVFile(String filename)
  {
    this.filename = filename;
    this.csv_file = null;
    readStockCSVFile();
  }

  private void readStockCSVFile()
  {
    File file = new File(this.filename);
    BufferedReader reader = null;
    BufferedReader reader2 = null;
    String[] number_of_columns = null;
    int i = 0;

    try
    {
      reader = new BufferedReader(new FileReader(file));
      String line;
      while ((line = reader.readLine()) != null)
      {
        if (i == 0)
          number_of_columns = line.split(",");
        i++;
      }
      reader.close();
      this.csv_file = new String[i][number_of_columns.length];
      reader2 = new BufferedReader(new FileReader(file));
      i = 0;
      while ((line = reader2.readLine()) != null)
      {
        String[] current = line.split(",");
        for (int j = 0; j < current.length; j++)
        this.csv_file[i][j] = current[j];
        i++;
      }
      reader2.close();
    }
    catch (Exception e)
    {
      System.err.format("Exception occured trying to read '%s'", this.filename);
      e.printStackTrace();
    }
  }

  public void saveFile()
  {
    File current = new File(this.filename);
    String old = this.filename + ".backup";
    current.renameTo(new File(old));
    File new_csv = new File(this.filename);
    try
    {
      BufferedWriter bw = new BufferedWriter(new FileWriter(new_csv));
      for (int i = 0; i < this.csv_file.length; i++)
      {
        for (int j = 0; j < this.csv_file[i].length; j++)
        {
          if (j < this.csv_file[i].length - 1)
          {
            if (this.csv_file[i][j] == null)
              bw.write(",");
            else
              bw.write(this.csv_file[i][j] + ",");
          }
          else
          {
            if (this.csv_file[i][j] == null)
              continue;
            bw.write(this.csv_file[i][j]);
          }
          bw.flush();
        }
        bw.write("\n");
        bw.flush();
      }

    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  private void changeValue(int i, int j, String value)
  {
    int l = 0, m = 0;
    if (i == 0 || j == 0 || i == 1)
    {
      System.out.println("You can't change that : " + this.csv_file[i][j]);
      this.csv_file = null;
    }
    else
    {
      String [][] new_csv_file = new String[this.csv_file.length][csv_file[0].length];
      for(String firstTab[] : csv_file)
      {
        for(String str : firstTab)
        {
          if (l == i && m == j)
          {
            new_csv_file[l][m] = value;
            if ((m > (this.csv_file[0].length - 1)) && (l < this.csv_file.length - 1))
            {
              l++;
              m = 0;
            }
            else
              m++;
            continue;
          }
          new_csv_file[l][m] = this.csv_file[l][m];
          m++;
        }
        m = 0;
        l++;
      }
      this.csv_file = new_csv_file;
    }
  }

  private void checkIfExist(int i, String[] name_and_param, int level)
  {
    String[] param;
    String[] param_values;
    int save_i = i;

    param = name_and_param[1].split(",");
    if (param.length >= 1)
    {
      for (int k = 0; k < param.length; k++)
      {
        if (this.csv_file == null)
          break;
        param_values = param[k].split(" ");
        if (param_values.length != 2)
        {
          System.out.println("Param_values.length : " + param_values.length);
          System.out.println("Format of the script : \"Name\"[,Level]:\"Column\" Value[,\"Column2\" Value2]\nYou can't have \":\" in your value");
          this.csv_file = null;
        }
        else
        {
          int j = 0;
          while (j < this.csv_file[0].length)
          {
            if (this.csv_file[0][j].trim().equals(param_values[0].trim()))
              break;
            j++;
          }
          if (j < this.csv_file[0].length)
          {
            if (level > 0)
            {
              int count = 2;
              int l = i + 1;
              while (this.csv_file[l][0].length() == 0)
              {
                l++;
                count++;
              }
              if (level <= count)
                i += level - 1;
              else
              {
                System.out.println("Bad level");
                this.csv_file = null;
                break;
              }
            }
            changeValue(i, j, param_values[1]);
            i = save_i;
          }
          else
          {
            System.out.println("Column " + param_values[0] + " doesn't exist");
            this.csv_file = null;
          }
        }
      }
    }
    else
    {
      System.out.println("Parameters should be >= 1");
      this.csv_file = null;
    }
  }

  public void splitAndSave(List<String> script_file)
  {
    Iterator iter = script_file.iterator();
    String[] name_and_param;
    String[] name_level;
    int i = 0;
    int level = 0;

    while (iter.hasNext())
    {
      if (this.csv_file == null)
        break;
      name_and_param = (((String)iter.next()).split(":"));
      if (name_and_param[0].indexOf(',') != -1)
      {
        name_level = name_and_param[0].split(",");
        if (name_level.length != 2)
        {
          System.out.println("Format of the script : \"Name\"[,Level]:\"Column\" Value[,\"Column2\" Value2]\nYou can't have \":\" in your value");
          this.csv_file = null;
          break;
        }
        name_and_param[0] = name_level[0];
        level = Integer.parseInt(name_level[1]);
        if (level < 1)
        {
          System.out.println("Level should be >= 1");
          this.csv_file = null;
          break;
        }
      }
      i = 0;
      while (i < this.csv_file.length)
      {
        if (this.csv_file[i][0].trim().equals(name_and_param[0].trim()))
          break;
        i++;
      }
      if (i < this.csv_file.length && name_and_param.length == 2)
        checkIfExist(i, name_and_param, level);
      else
      {
        if (name_and_param.length < 2)
          System.out.println("Format of the script : \"Name\"[,Level]:\"Column\" Value[,\"Column2\" Value2]\nYou can't have \":\" in your value");
        else if (name_and_param.length > 2)
          System.out.println("You can't have \":\" in your value");
        else if (i >= this.csv_file.length)
          System.out.println("You try to change values of a wrong name");
        this.csv_file = null;
      }
    }
  }

  public String[][] getCSVFile()
  {
    return this.csv_file;
  }
}
