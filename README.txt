Clash CSV Modder

Description
Clash CSV Modder is a JAVA's program which uses a script file for modding Clash of Clans CSV files automatically, to avoid having to edit them manually.
If Supercell decides to modify a CSV file, this program will allow us not to have to modify the file all over again.
This can be used to create CSV patch for private servers such as Ultrapowa Clash Server (ultrapowa.com).
The program will create a backup for CSV file, and replace values in the CSV file with script file value.
Script is a .txt file that you'll write, the format is :

  "Name"[,Level]:"Column" value[,"Column1" value2]

Level is optional and number of spaces is very important. You must include at least 1 column. This is an example of script file:

  "Test_4":"PreparationMinutes" 3,"WarMinutes" 5,"DisableProduction" "false"
  "Small_1":"PreparationMinutes" 6,"WarMinutes" 5
  "Large_2":"PreparationMinutes" 0,"WarMinutes" 2,"TeamSize" 42
  "Epic_3":"PreparationMinutes" 2,"WarMinutes" 4
  "Medium_1":"TeamSize" 20

An other:

  "Barbarian",3:"LaboratoryLevel" 4
  "Goblin",2:"LaboratoryLevel" 5
  "Archer",3:"Hitpoints" 1000,"UpgradeTimeH" 8000
  "Giant",6:"TrainingCost" 4242
  "Balloon",4:"UpgradeCost" 0
  "Wizard",1:"TrainingTime" 5,"Speed" 20


How to launch the program:

  java -jar interpret.jar script_file csv_file


Where:
script_file: name of your .txt file (for example : script.txt)
csv_file: the name of the csv file to modify (for example : characters.csv)


-----

Description
Clash CSV Modder est un programme JAVA qui prend un fichier script pour modifier un fichier CSV automatiquement et ainsi éviter de les éditer à la main.
Ainsi si Supercell modifie un fichier CSV, ça permettra d'éviter d'avoir à refaire tout le fichier.
Cela peut être utilisé pour éditer des fichiers CSV pour des serveurs privés tels que Ultrapowa Clash Server (ultrapowa.com)
Le programme crée une sauvegarde de la version précédente de votre fichier CSV et remplace les valeurs par celles dans le script.
Le script est un fichier .txt que vous devez écrire, le format est le suivant :

  "Name"[,Level]:"Column" value[,"Column1" value2]

Le level est optionnel et les nombres d'espaces sont très importants. Vous devez mettre au moins une colonne. Voici un exemple d'un fichier script :

  "Test_4":"PreparationMinutes" 3,"WarMinutes" 5,"DisableProduction" "false"
  "Small_1":"PreparationMinutes" 6,"WarMinutes" 5
  "Large_2":"PreparationMinutes" 0,"WarMinutes" 2,"TeamSize" 42
  "Epic_3":"PreparationMinutes" 2,"WarMinutes" 4
  "Medium_1":"TeamSize" 20
  "Barbarian",3:"LaboratoryLevel" 4

Un autre :

  "Barbarian",3:"LaboratoryLevel" 4
  "Goblin",2:"LaboratoryLevel" 5
  "Archer",3:"Hitpoints" 1000,"UpgradeTimeH" 8000
  "Giant",6:"TrainingCost" 4242
  "Balloon",4:"UpgradeCost" 0
  "Wizard",1:"TrainingTime" 5,"Speed" 20

Pour lancer le programme :

  java -jar interpret.jar script_file csv_file

Où :
script_file est le nom de votre fichier .txt (par example : script.txt)
csv_file le nom du fichier CSV à modifier (par exemple : characters.csv)
